//
//  PurchaseUpgradeViewController.h
//  TopXNotes
//
//  Created by Lewis Garrett on 3/26/14.
//  Copyright Tropical Software 2014. All rights reserved.
//
//

#import <UIKit/UIKit.h>

@interface PurchaseUpgradeViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    
    IBOutlet UIButton *doneButton;
    IBOutlet UIButton *viewUpgradeFeaturesButton;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)dismissAction:(id)sender;
- (IBAction)showUpgradeFeaturesAction:(id)sender;

@end
