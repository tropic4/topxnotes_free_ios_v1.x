//
//  UUID.m
//  TopXNotes
//
//  Created by Lewis E. Garrett on 3/27/12.
//  Copyright (c) 2012 Iota. All rights reserved.
//

#import "UUID.h"

@implementation NSString (UUID)

+ (NSString *)uuid
{
    NSString *uuidString = nil;
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    if (uuid) {
        uuidString = (NSString *)CFBridgingRelease(CFUUIDCreateString(NULL, uuid));
        CFRelease(uuid);
    }
    return uuidString;
}

@end