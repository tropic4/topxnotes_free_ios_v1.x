//
//  UpgradeFeaturesViewController.m
//  TopXNotes
//
//  Created by Lewis Garrett on 10/2/14.
//
//

#import "UpgradeFeaturesViewController.h"

@interface UpgradeFeaturesViewController ()

@end

@implementation UpgradeFeaturesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    NSURL *url = [NSURL URLWithString:@"http://www.tropic4.com"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
#pragma unused (interfaceOrientation)
    // Return YES for supported orientations
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buyTopXNotes:(id)sender {
#pragma unused (sender)

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/topxnotes/id368084877?mt=8"]];
}

- (IBAction)purchaseTopXNotesPro:(id)sender {
#pragma unused (sender)

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/topxnotes/id368084877?mt=8"]];
}
@end
