//
//  UpgradeFeaturesViewController.h
//  TopXNotes
//
//  Created by Lewis Garrett on 10/2/14.
//
//

#import <UIKit/UIKit.h>

@interface UpgradeFeaturesViewController : UIViewController {

IBOutlet UIWebView	*webView;

    __weak IBOutlet UIButton *buyFromAppStore;
}
- (IBAction)buyTopXNotes:(id)sender;
- (IBAction)purchaseTopXNotesPro:(id)sender;

@end
