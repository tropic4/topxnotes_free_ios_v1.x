//
//  NotePadViewController.m
//  NotesTopX
//
//  Created by Lewis Garrett on 4/11/09.
//  Copyright 2009 Iota. All rights reserved.
//

#import "AboutViewController.h"                                                 //leg202100726 - TopXNotesFree 1.6
#import "NotePadViewController.h"
#import "NoteViewController.h"
#import "Formatter.h"
#import "Constants.h"
#import "Model.h"
#import "Note.h"
#import "NoteListCell.h"
#import "NSMutableString+EmailEncodingExtensions.h"
//#import "BannerViewController.h"
//#import <StoreKit/StoreKit.h>                                                   //leg20140325 - Free 1.5.0
//#import "TopXNotesIAPHelper.h"                                                  //leg20140325 - Free 1.5.0
#import "UIDeviceHardware.h"                                                    //leg20210818 - TopXNotesFree 1.6

@interface NotePadViewController () {
    NSArray *_products;                                                         //leg20140325 - Free 1.5.0
    NSNumberFormatter * _priceFormatter;                                        //leg20140325 - Free 1.5.0
}

@property (nonatomic, strong) IBOutlet UIView *contentView;                     //leg20140320 - Free 1.5.0

@end

@implementation NotePadViewController {
    ADBannerView *_bannerView;                                                  //leg20140320 - Free 1.5.0
}

@synthesize model;
@synthesize toolBar;  //leg20121017 - 1.2.2

#pragma mark - View Control

// Reload the note list table, properly sorted          //leg20121022 - 1.2.2
- (void)reloadData
{
    switch (sortType) {
        case NotesSortByDateAscending:
            [model sortNotesByDate:YES];	// sort date ascending
            break;
            
        case NotesSortByDateDescending:
            [model sortNotesByDate:NO];		// sort date descending
            break;
            
        case NotesSortByTitleAscending:
            [model sortNotesByTitle:YES];	// sort title ascending
            break;
            
        case NotesSortByTitleDescending:
            [model sortNotesByTitle:NO];	// sort title descending
            break;
            
        default:
            break;
    }
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	// Establish Edit button as right button of navigation bar
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    // Log information about the device and make sure the model gets saved.     //leg20120313 - 1.2.0
	NSString *tabTitle = self.navigationItem.title;
//	NSComparisonResult compareResult = [tabTitle compare:@"TopXNotes"];
    
    // Reading Defaults                                                         //leg20121017 - 1.2.2
	NSUserDefaults *defaults;
	defaults = [NSUserDefaults standardUserDefaults];
    
	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict != nil)
		savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
	else
		savedSettingsDictionary = [NSMutableDictionary dictionary];
    
	// Get current note sort type
    NSNumber *sortTypeNumber;
    if (!(sortTypeNumber = [savedSettingsDictionary objectForKey:kSortType_Key])) {
        // Set default sort type.
        sortTypeNumber = [NSNumber numberWithInt:kDefaultSortType];
    }
    
    [savedSettingsDictionary setObject:sortTypeNumber forKey:kSortType_Key];
    
    // Save settings
    [[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Set sort type
	sortType = [[savedSettingsDictionary objectForKey:kSortType_Key] intValue];
	
        
// Change the way sort controls are displayed.  Through 1.2.6 the controls      //leg20140205 - 1.2.7
//  were placed in a toolbar inside the navigation bar's title.  Beginning
//  iOS 7, this display became quite ugly.  Therefore, changed to using
//  the NavigationController's inherent toolbar to display the sort
//  controls as text buttons.

    // Add ability to sort note list by Title or Date.                          //leg20121017 - 1.2.2
    
    // Button to control type of sort.
    UIBarButtonItem * sortFieldBarButtonItem = [[UIBarButtonItem alloc]
                                                initWithTitle:@"Sort by Date" style:UIBarButtonItemStyleBordered target:self action:@selector(sortControlHit:)];
    [sortFieldBarButtonItem setTag:1];
    
    // Button to control direction of sort.
    UIBarButtonItem * sortDirectionBarButtonItem = [[UIBarButtonItem alloc]
                                                initWithTitle:@"Ascending" style:UIBarButtonItemStyleBordered target:self action:@selector(sortControlHit:)];
    [sortDirectionBarButtonItem setTag:2];
    
    // Spacer button
    UIBarButtonItem *flexiableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    self.navigationController.toolbarHidden=NO;
    self.navigationController.toolbar.barStyle = UIBarStyleBlack;
    self.navigationController.toolbar.translucent = YES;
//    if (IS_OS_7_OR_LATER)                                                       //leg20140205 - 1.2.7
//        self.navigationItem.title = @"";    // Blank title area for iOS 7.
   
    self.toolbarItems = [NSArray arrayWithObjects:flexiableItem, sortFieldBarButtonItem, flexiableItem, sortDirectionBarButtonItem, flexiableItem, nil];
    
    
    switch (sortType) {
        case NotesSortByDateAscending:
            [sortFieldBarButtonItem setTitle:@"Sort by Date"];
            [sortDirectionBarButtonItem setTitle:@"Ascending"];
            break;
            
        case NotesSortByDateDescending:
            [sortFieldBarButtonItem setTitle:@"Sort by Date"];
            [sortDirectionBarButtonItem setTitle:@"Descending"];
            break;
            
        case NotesSortByTitleAscending:
            [sortFieldBarButtonItem setTitle:@"Sort by Title"];
            [sortDirectionBarButtonItem setTitle:@"Ascending"];
            break;
            
        case NotesSortByTitleDescending:
            [sortFieldBarButtonItem setTitle:@"Sort by Title"];
            [sortDirectionBarButtonItem setTitle:@"Descending"];
            break;
            
        default:
            break;
    }
    	
	// If we are in "TopXNotes" tab, do the auto backup process
//	if (compareResult == NSOrderedSame) {
    if ([tabTitle hasPrefix:@"TopXNotes"]) {                                    //leg20210715 - TopXNotesFree 1.6

		// Get iPhone device information
		UIDevice *device = [UIDevice currentDevice];
        //NSString *uniqueIdentifier = [device uniqueIdentifier];

        // Use our generated unique identifier as a replacement for the deprecated  //leg20130514 - 1.2.6
		//  [device uniqueIdentifier].
//        NSUserDefaults *defaults;
//		defaults = [NSUserDefaults standardUserDefaults];
//		NSString *uniqueIdentifier = [defaults objectForKey:kUUID_GENERATED_KEY];
        NSString *uniqueIdentifier = [model deviceUDID];                    //leg20140116 - 1.2.6
        
		NSString *name = [device name];
		NSString *systemName = [device systemName];
		NSString *localizedModel = [device localizedModel];
		NSString *deviceModel = [device model];
		NSString *systemVersion = [device systemVersion];
		NSLog(@"Device UDID: %@, Model: %@, Localized Model: %@, Name: %@", uniqueIdentifier, deviceModel, localizedModel, name);
		NSLog(@"System Name: %@, System Version: %@", systemName, systemVersion);
        
// UDID is now set in the TopXNotesAppDelegate.                                 //leg20140116 - 1.2.6
//		// Set the device UDID (used for identifying which iDevice is syncing with Mac)
//		[model setDeviceUDID:uniqueIdentifier];
//		
//		// Set the real device UDID so that when Apple removes the deprecated: [device uniqueIdentifier] that
//      //  we will be able to convert it to the generated UDID in the Mac datafile.
//		[model setRealDeviceUDID:uniqueIdentifier];
        
        // Fix notepad by clearing nil objects                                  //leg20131204 - 1.2.6
        if ([[model notePadVersion] integerValue] <= kNotePadVersion3) {
            for (int i=0; i < [model numberOfNotes]; i++) {                     //leg20130314 - 1.2.3
                Note *note = [model	getNoteForIndex:i];
//                BOOL noteChanged = NO;
                if (note.noteID == nil) {
                    note.noteID = [NSNumber numberWithUnsignedLong:0];
//                    noteChanged = YES;
                }
                if (note.groupID == nil) {
                    note.groupID = [NSNumber numberWithUnsignedLong:0];
//                    noteChanged = YES;
                }
                if (note.syncFlag == nil) {
                    note.syncFlag = [NSNumber numberWithBool:NO];
//                    noteChanged = YES;
                }
                if (note.needsSyncFlag == nil) {
                    note.needsSyncFlag = [NSNumber numberWithBool:NO];
//                    noteChanged = YES;
                }
                [model replaceNoteAtIndex:i withNote:note];
            }
        }

		// Must save model here in case this was a new notepad with no notes
        //  in it yet, else there is no representation on disk which screws
        //  Sync up.
		[model saveData];					// Insure deviceUDID is saved	

        // Fix Issue #004 - "Navigation Bar overlaid by Status Bar preventing   //leg202100726 - TopXNotesFree 1.6
        //  backward navigation in some views"
        //  Present a modal view at notepad start-up so that navigation bar not
        //  overlaid by status bar in some views.
        if (@available(iOS 13.0, *)) {
            AboutViewController    *modalAboutViewController = [[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
            modalAboutViewController.model = self.model;
            modalAboutViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;

            // On iOS 13 or greater must force full screen presentation if
            //  views are in .xibs.
            modalAboutViewController.modalPresentationStyle = UIModalPresentationFullScreen;
            
            [[self navigationController] presentViewController:modalAboutViewController animated:YES completion:nil];
        }
    }
    
    [self reloadData];      //leg20121022 - 1.2.2

//    // On iOS 6 ADBannerView introduces a new initializer, use it when available.   //leg20140320 - Free 1.5.0
//    if ([ADBannerView instancesRespondToSelector:@selector(initWithAdType:)]) {
//        _bannerView = [[ADBannerView alloc] initWithAdType:ADAdTypeBanner];
//    } else {
//        _bannerView = [[ADBannerView alloc] init];
//    }
//    _bannerView.delegate = self;
//    
//    [self.view addSubview:_bannerView];                                         //leg20140320 - Free 1.5.0


//    // Get IAP list of possible products.                                       //leg20140325 - Free 1.5.0
//    [self loadProducts];
//    
//    // Add to end of viewDidLoad
//    _priceFormatter = [[NSNumberFormatter alloc] init];
//    [_priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
//    [_priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
//    
//    // Add to end of viewDidLoad
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Restore" style:UIBarButtonItemStyleBordered target:self action:@selector(restoreTapped:)];
}


//// Get our In-App Purchase list of products into an array.                      //leg20140325 - Free 1.5.0
//- (void)loadProducts
//{
//    _products = nil;
//    [[TopXNotesIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
//        if (success) {
//            _products = products;
//        }
//    }];
//}

- (void)viewWillAppear:(BOOL)animated {
#pragma unused (animated)

    [super viewWillAppear:animated];                                            //leg20150114 - Free 1.5.0

	// Refresh note list
	[self.tableView reloadData];
	
    // Fix and Restore Notes tab badge value update                             //leg20210715 - TopxNotesFree 1.6
	// Set Notes tab badge value to the number of un-synced notes.				//leg20110421 - 1.0.4
//	 // Remove for now the badge value update from Notes tab.					//leg20110523 - 1.0.4
    [self updateBadgeValue];

// Moved this code from -viewDidLoad.  The code would not be executed after the	//leg20110614 - 1.1.0
//	initial start-up of the App on a device that background/resumes Apps.  This
//	insures that backups are taken when the notepad changes.

	// Check to see if we are in the "TopXNotes" tab.
	//	This check is necessary to keep from doing the auto backup process a
	//	second time after startup.  This is because NotePadView is reused
	//	when the "Email" tab is selected and so viewDidLoad can be entered
	//	a second time.  
	NSString *tabTitle = self.navigationItem.title;
//	NSComparisonResult compareResult = [tabTitle compare:@"TopXNotes"];
	
	// If we are in "TopXNotes" tab, do the auto backup process
//	if (compareResult == NSOrderedSame) {
    if ([tabTitle hasPrefix:@"TopXNotes"]) {                                    //leg20210715 - TopXNotesFree 1.6
	    //noteText.font = [UIFont fontWithName:kNoteTextFont size:kNoteTextFontSize];

		// Reading Defaults 
		NSUserDefaults *defaults;
		defaults = [NSUserDefaults standardUserDefaults];
		
		NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
		if (dict != nil) 
			savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
		else
			savedSettingsDictionary = [NSMutableDictionary dictionary];

		// Get Font defaults																//leg20110416 - 1.0.4
		NSString * fontName = [savedSettingsDictionary objectForKey: kNoteFontName_Key];
		if (fontName == nil) {
			fontName = kNoteTextFont;
			[savedSettingsDictionary setObject:fontName forKey:kNoteFontName_Key];
		}
		NSString * fontSize = [savedSettingsDictionary objectForKey: kNoteFontSize_Key];
		if (fontSize == nil) {
			fontSize = [NSString stringWithFormat:@"%d", kNoteTextFontSize];
			[savedSettingsDictionary setObject:fontSize forKey:kNoteFontSize_Key];
		}

		// Get Sync settings.																//leg20110502 - 1.0.4
		NSNumber * syncEnabledOrDisabled = [savedSettingsDictionary objectForKey: kSyncEnabledOrDisabled_Key];
		if (syncEnabledOrDisabled == nil) {
			syncEnabledOrDisabled = [NSNumber numberWithBool:NO];
			[savedSettingsDictionary setObject:syncEnabledOrDisabled forKey:kSyncEnabledOrDisabled_Key];
		}

		// Get auto notepad backup data
		NSMutableArray *array = [dict objectForKey: kAutoBackupsArray_Key];
		if (array != nil) 
			autoBackupsArray = [NSMutableArray arrayWithArray:array];
		else
			autoBackupsArray = [NSMutableArray array];

// Moved this code from -viewDidLoad.                                       //leg20120313 - 1.2.0
//
//		// Get iPhone device information
//		UIDevice *device = [UIDevice currentDevice];
//		NSString *uniqueIdentifier = [device uniqueIdentifier];
//		NSString *name = [device name];
//		NSString *systemName = [device systemName];
//		NSString *localizedModel = [device localizedModel];
//		NSString *deviceModel = [device model];
//		NSString *systemVersion = [device systemVersion];
//		NSLog(@"Device UDID: %@, Model: %@, Localized Model: %@, Name: %@", uniqueIdentifier, deviceModel, localizedModel, name);
//		NSLog(@"System Name: %@, System Version: %@", systemName, systemVersion);
//
//		// Set the device UDID (used for identifying which iDevice is syncing with Mac)
//		[model setDeviceUDID:uniqueIdentifier];
//		
//		// Stop saving the model here.  It was causing backup of notepad every time -viewWillAppear		//leg20110614 - 1.1.0
//		//  even though no notes had changed.															
//		//[model saveData];					// Insure deviceUDID is saved								
		
		// Initialize the next backup number
		nextAutoBackupNumber = [dict objectForKey: kNextAutoBackupNumber_Key];
		NSInteger nextBackupNumber = 0;
		if (nextAutoBackupNumber == nil)  {
			nextBackupNumber = 1;			// first run
		} else { 
			nextBackupNumber = [nextAutoBackupNumber intValue];
		}
		
		// Prepare to copy notepad
		NSFileManager *fileManager = [NSFileManager defaultManager];
		NSError *error = nil;
		NSString *pathToModelDataFile = [model pathToData];
		NSString *nextBackupFileName = [NSString stringWithFormat:@"%ld_%@", (long)nextBackupNumber, kBackupModelFileName];
		NSString *pathToBackupModelDataFile = [self pathToAutoBackup:nextBackupFileName];
		NSInteger lastBackupIndex;
		NSString *lastBackupFileName;										
		NSString *pathToLastBackupModelDataFile;
		NSDictionary *fileAttributesDictionary;
		NSDate *nextBackupModificationDate;
		NSDate *lastBackupModificationDate;
		BOOL result = NO;
		
		if ([autoBackupsArray count] > 0) {
			lastBackupIndex  = [autoBackupsArray count] < kNumberOfAutoBackups ? [autoBackupsArray count]-1 : kNumberOfAutoBackups-1;
			lastBackupFileName = [autoBackupsArray objectAtIndex:lastBackupIndex];										
			pathToLastBackupModelDataFile = [self pathToAutoBackup:lastBackupFileName];

			// Get the modification date of the current notepad (would be next backup)
			fileAttributesDictionary = [fileManager attributesOfItemAtPath:pathToModelDataFile error:&error];
			nextBackupModificationDate = [fileAttributesDictionary fileModificationDate];

			// Get the modification date of the last backup
			fileAttributesDictionary = [fileManager attributesOfItemAtPath:pathToLastBackupModelDataFile error:&error];
			lastBackupModificationDate = [fileAttributesDictionary fileModificationDate];
			
			// if the modification date of the last backup matches that of the current notepad, no need to back it up 
			if ([nextBackupModificationDate isEqualToDate:lastBackupModificationDate])
				return;
		}

		// Make a backup copy of the notepad
		[fileManager removeItemAtPath:pathToBackupModelDataFile error:NULL];
		[fileManager copyItemAtPath:pathToModelDataFile toPath:pathToBackupModelDataFile error:&error];
		if ([error code] != 0) {
			NSLog(@"Error making a backup of notepad file! Error code=%ld", (long)[error code]);
			return;
		}
		
		// Set the modification date of the backed-up notepad to the same as the current notepad model.		//leg20110614 - 1.1.0
		fileAttributesDictionary = [fileManager attributesOfItemAtPath:pathToModelDataFile error:&error];		//leg20110629 - 1.1.0
		nextBackupModificationDate = [fileAttributesDictionary fileModificationDate];							//leg20110629 - 1.1.0
		fileAttributesDictionary = [NSDictionary dictionaryWithObject:nextBackupModificationDate forKey:NSFileModificationDate];
		result = [fileManager setAttributes:fileAttributesDictionary ofItemAtPath:pathToBackupModelDataFile error:&error];
		if ([error code] != 0 || !result) {
			NSLog(@"Error setting modification date of backup notepad file! Error code=%ld", (long)[error code]);
			return;
		}

		// Add the backup to the list of backups
		[autoBackupsArray addObject:nextBackupFileName];

		// If backups list is not full yet, just increment to next backup number
		if ([autoBackupsArray count] < kNumberOfAutoBackups) {
			nextAutoBackupNumber = [NSNumber numberWithInteger:++nextBackupNumber];
		} else {
		
			// Backups list is full, reset backup number and get rid of oldest backup
			if (nextBackupNumber < kNumberOfAutoBackups) {
				nextAutoBackupNumber = [NSNumber numberWithInteger:++nextBackupNumber];
			} else {
			
				// start back with 1st backup number
				nextAutoBackupNumber = [NSNumber numberWithInt:1];		
			}
			
			// remove oldest backup
			if ([autoBackupsArray count] > kNumberOfAutoBackups)
				[autoBackupsArray removeObjectAtIndex:(NSUInteger)0];	
		}

		// Update the backup settings
		[savedSettingsDictionary setObject:nextAutoBackupNumber forKey:kNextAutoBackupNumber_Key];
		[savedSettingsDictionary setObject:autoBackupsArray forKey:kAutoBackupsArray_Key];

		// Save settings
		[[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
}

- (void)viewDidDisappear:(BOOL)animated                                         //leg20140320 - Free 1.5.0
{
    [super viewDidDisappear:animated];
//    [self stopTimer];
}

- (void)viewWillDisappear:(BOOL)animated
{
#pragma unused (animated)
    
    [super viewWillDisappear:animated];                                         //leg20150114 - Free 1.5.0
    
}

- (NSUInteger)supportedInterfaceOrientations                                    //leg20140320 - Free 1.5.0
{
    return UIInterfaceOrientationMaskAll;
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//#pragma unused (interfaceOrientation)
//
//    // Return YES for supported orientations
//
//    return YES;
//}

#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_6_0
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation //leg20140320 - Free 1.5.0
{
#pragma unused (interfaceOrientation)
    
    return YES;
}
#endif

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
                                     // Release anything that's not essential, such as cached data
    NSLog(@"NotePadViewController didReceiveMemoryWarning");
}

//#pragma mark - iAd
//
//- (void)viewDidLayoutSubviews                                                   //leg20140320 - Free 1.5.0
//{
//    [self layoutAnimated:[UIView areAnimationsEnabled]];
//}
//
//- (void)bannerViewDidLoadAd:(ADBannerView *)banner
//{
//#pragma unused (banner)
//    
//    self.navigationController.toolbarHidden=YES;
//    [self layoutAnimated:YES];
//}
//
//- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error  //leg20140320 - Free 1.5.0
//{
//#pragma unused (error, banner)
//    
//    [self layoutAnimated:YES];
//}
//
//- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave //leg20140320 - Free 1.5.0
//{
//#pragma unused (banner, willLeave)
//    
//    //    [self stopTimer];
//    return YES;
//}
//
//- (void)bannerViewActionDidFinish:(ADBannerView *)banner                        //leg20140320 - Free 1.5.0
//{
//#pragma unused (banner)
//    
//    self.navigationController.toolbarHidden=NO;
//    //    [self startTimer];
//}
//
//- (void)layoutAnimated:(BOOL)animated                                           //leg20140320 - Free 1.5.0
//{
//    // As of iOS 6.0, the banner will automatically resize itself based on its width.
//    // To support iOS 5.0 however, we continue to set the currentContentSizeIdentifier appropriately.
//    CGRect contentFrame = self.view.bounds;
//    if (contentFrame.size.width < contentFrame.size.height) {
//        _bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
//    } else {
//        _bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
//    }
//    
//    CGRect bannerFrame = _bannerView.frame;
//    if (_bannerView.bannerLoaded) {
//        contentFrame.size.height -= _bannerView.frame.size.height;
//        bannerFrame.origin.y = contentFrame.size.height;
//    } else {
//        bannerFrame.origin.y = contentFrame.size.height;
//    }
//    
//    [UIView animateWithDuration:animated ? 0.25 : 0.0 animations:^{
//        _contentView.frame = contentFrame;
//        [_contentView layoutIfNeeded];
//        _bannerView.frame = bannerFrame;
//    }];
//}

#pragma mark - Table Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#pragma unused (tableView)

    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#pragma unused (tableView, section)

    return [model numberOfNotes];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
	NoteListCell *cell = (NoteListCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil)
	{
//		cell = [[[NoteListCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
        cell = [[NoteListCell alloc]
                 initWithReuseIdentifier:CellIdentifier];          //leg20140205 - 1.2.7
	}
		
    int row = (int)indexPath.row;
    Note* note = [model getNoteForIndex: row];
    
	// Check to see if it is a synced note
	//if ([note.noteID unsignedLongValue] == 0)
    if ([note.needsSyncFlag unsignedLongValue] == 0)                            //leg20130918 - 1.2.6
		cell.dateLabel.textColor = [UIColor blueColor];		// Show it is not a synced note
	else
		cell.dateLabel.textColor = [UIColor redColor];		// Show it is a synced note

    // Display title and modification date.
	NSString* dateString = [NSString longDate:note.date];
	cell.noteTitleLabel.text = note.title;
	cell.dateLabel.text = dateString;
    
//    // Display creation date in 2-line format for testing purposes.
//	NSString* dateString2 = [NSString longDate:note.createDate];
//    cell.infoLabel.text = dateString2;

//    if (IS_OS_7_OR_LATER)                                                      //leg20140205 - 1.2.7
//        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
	
	return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle 
	forRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma unused (tableView)
	
	if (editingStyle == UITableViewCellEditingStyleDelete) {
        int row = (int)indexPath.row;
		[model removeNoteAtIndex:row];
		
		[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] 
		                      withRowAnimation:UITableViewRowAnimationFade];

        // Fix and Restore Notes tab badge value update                         //leg20210715- TopxNotesFree 1.6
        // Insure tab item badge value is updated.                              //leg20110421 - 1.0.4
        // Remove for now the badge value update from Notes tab.                //leg20110523 - 1.0.4
        [self updateBadgeValue];
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma unused (tableView)

    int row = (int)indexPath.row;
	Note *note = [model getNoteForIndex:row];


	// If in the Email tab then send the note to Mail,
	//	else, put the note in a NoteView
	NSString *tabTitle = self.navigationItem.title;
	NSComparisonResult compareResult = [tabTitle compare:@"Email"];

	if (compareResult == NSOrderedSame) {
		noteIndex = row;
		
		// The response to this dialog is processed in alertOKCancelAction delegate -alertView (sendEmail)
		UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat: @"Email note \"%@\"?", note.title]
				delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Proceed" otherButtonTitles:nil];
		actionSheet.actionSheetStyle = UIActionSheetStyleDefault;

		// Show the dialog coming out of the tab bar		
		UITabBar *tabBar = self.tabBarController.tabBar;
		[actionSheet showFromTabBar:tabBar];
		
	} else {
		NoteViewController *controller = [[NoteViewController alloc] 
										   initWithNibName:@"NoteView" noteIndex:row];
										   
		controller.model = self.model;

		[self.navigationController pushViewController:controller animated:YES];
	}
}

#pragma mark - Private Methods

// Sort type changed.                      //leg20121022 - 1.2.2

// Change the way sort controls are displayed.  Through 1.2.6 the controls      //leg20140205 - 1.2.7
//  were placed in a toolbar inside the navigation bar's title.  Beginning
//  iOS 7, this display became quite ugly.  Therefore, changed to using
//  the NavigationController's inherent toolbar to display the sort
//  controls as text buttons.

- (void)sortControlHit:(id)sender {
    
    UIBarButtonItem * sortButtonItem = sender;
    
    switch (sortButtonItem.tag) {
            
        case 1:     // sort field
        {
            switch (sortType) {
                case NotesSortByDateAscending:
                    sortType = NotesSortByTitleAscending;
                    [sortButtonItem setTitle:@"Sort by Title"];
                    break;
                    
                case NotesSortByDateDescending:
                    sortType = NotesSortByTitleDescending;
                    [sortButtonItem setTitle:@"Sort by Title"];
                    break;
                    
                case NotesSortByTitleAscending:
                    sortType = NotesSortByDateAscending;
                    [sortButtonItem setTitle:@"Sort by Date"];
                    break;
                    
                case NotesSortByTitleDescending:
                    sortType = NotesSortByDateDescending;
                    [sortButtonItem setTitle:@"Sort by Date"];
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case 2:
        {
            switch (sortType) {
                case NotesSortByDateAscending:
                    sortType = NotesSortByDateDescending;
                    [sortButtonItem setTitle:@"Descending"];
                    break;
                    
                case NotesSortByDateDescending:
                    sortType = NotesSortByDateAscending;
                    [sortButtonItem setTitle:@"Ascending"];
                    break;
                    
                case NotesSortByTitleAscending:
                    sortType = NotesSortByTitleDescending;
                    [sortButtonItem setTitle:@"Descending"];
                    break;
                    
                case NotesSortByTitleDescending:
                    sortType = NotesSortByTitleAscending;
                    [sortButtonItem setTitle:@"Ascending"];
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
    
    
	// Reading Defaults
	NSUserDefaults *defaults;
	defaults = [NSUserDefaults standardUserDefaults];
    
	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict != nil)
		savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
	else
		savedSettingsDictionary = [NSMutableDictionary dictionary];
    
	// Update the Font settings in user defaults
	[savedSettingsDictionary setObject:[NSNumber numberWithInteger:sortType] forKey:kSortType_Key];
	
	// Save settings
	[[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [self reloadData];
}

//// Update the tab bar item badge value											//leg20110421 - 1.0.4
//- (void)updateBadgeValue {
//	NSString *tabTitle = self.navigationItem.title;
//	NSComparisonResult compareResult = [tabTitle compare:@"TopXNotes"];
//
//	// If we are in "TopXNotes" tab, set the badge.
//	if (compareResult == NSOrderedSame) {
//		int unSyncedNotesCount = 0;
//		for (int i=0; i < [model numberOfNotes]; i++) {
//			Note *note = [model	getNoteForIndex:i];
//			if ([note.noteID unsignedLongValue] == 0)
//				unSyncedNotesCount++;
//		}
//
//		if (unSyncedNotesCount > 0)
//			self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%d", unSyncedNotesCount];
//		else
//			self.navigationController.tabBarItem.badgeValue = nil;
//	}
//}

// Fix and Restore Notes tab badge value update                                 //leg20210715 - TopxNotesFree 1.6
// Update the tab bar item badge value.
- (void)updateBadgeValue {
    NSString *tabTitle = self.navigationItem.title;

    // If we are in "TopXNotes" tab, set the badge.                             //leg20210708 - TopxNotes 1.6
    if ([tabTitle hasPrefix:@"TopXNotes"]) {                                    //leg20210713 - TopXNotes2
        int unSyncedNotesCount = 0;
        for (int i=0; i < [model numberOfNotes]; i++) {
            Note *note = [model    getNoteForIndex:i];
            if ([note.needsSyncFlag boolValue])                                 //leg20210708 - TopxNotes 1.6
                unSyncedNotesCount++;
        }

        if (unSyncedNotesCount > 0)
            self.tabBarController.tabBar.selectedItem.badgeValue = [NSString stringWithFormat:@"%d", unSyncedNotesCount];   //leg20210713 - TopXNotes2
        else
            self.tabBarController.tabBar.selectedItem.badgeValue = nil;         //leg20210713 - TopXNotes2
    }
}

- (NSString*)pathToAutoBackup:(NSString*)withFileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";  
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:withFileName];

	return appFile;
}


// old way of sending Email before MFMailComposeViewController in iPhone OS 3.0
- (void)launchMailAppOnDevice
{
	Note *note = [model getNoteForIndex:noteIndex];

	// Assemble the Email
	NSString *subjectPrefix = @"Re: TopXNotes Note --> \"";
	NSString *completeSubject = [subjectPrefix stringByAppendingString:note.title];
	completeSubject = [completeSubject stringByAppendingString:@"\""];
	
	NSMutableString *subject = [NSMutableString stringWithString:completeSubject];
	NSMutableString *body = [NSMutableString stringWithString:note.noteText];

	// encode the strings for email
	[subject encodeForEmail];

	[body replaceOccurrencesOfString:@"\r" withString:@"<br />" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [body length])];	// Make sure CRs are recognized
	[body encodeForEmail];

	NSString *emailMsg = [NSMutableString stringWithFormat:@"mailto:?subject=%@&body=%@", subject, body];
	NSURL *encodedURL = [NSURL URLWithString:emailMsg];
		 
	// Send the assembled message to Mail!
	[[UIApplication sharedApplication] openURL:encodedURL];
}


- (void)sendEmail
{

	// This can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
//	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
//	if (mailClass != nil)
    // Fix Issue #001 - Keyboard messed up on iPad Pro only                     //leg20210818 - TopXNotesFree 1.6
    //  For iPad Pro default to old Email method - launch Mail instead of using
    //  mail composer class.
    NSString *model = [[[UIDeviceHardware alloc] init] platformString];
    Boolean IsIPadPro = CFStringHasPrefix((CFStringRef)model, (CFStringRef)@"iPad Pro");
    NSLog(@"Current device is: %@", model);                                     //leg20210818 - TopXNotesFree 1.6

    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil && !IsIPadPro)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self displayComposerSheet];
		}
		else
		{
			[self launchMailAppOnDevice];
		}
	}
	else
	{
		[self launchMailAppOnDevice];
	}
}

-(void)displayComposerSheet 
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
        
	Note *note = [model getNoteForIndex:noteIndex];

	// Assemble the Email - Don't set any recipients, let user do during composition
	NSString *subjectPrefix = @"Re: TopXNotes Note --> \"";
	NSString *completeSubject = [subjectPrefix stringByAppendingString:note.title];
	completeSubject = [completeSubject stringByAppendingString:@"\""];
	
	NSMutableString *subject = [NSMutableString stringWithString:completeSubject];
	NSMutableString *body = [NSMutableString stringWithString:note.noteText];
	[body replaceOccurrencesOfString:@"\r" withString:@"<br />" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [body length])];	// Make sure CRs are recognized
	
	[picker setSubject:subject];
	[picker setMessageBody:body isHTML:YES];		// Say it is HTML so CRs are recognized
	
	// Present the composer
    [self presentViewController:picker animated:YES completion:nil];            //leg20140205 - 1.2.7
	
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{       
#pragma unused (controller, error)

	NSString *resultMessage;
        // Notifies users about errors associated with the interface
        switch (result)
        {
                case MFMailComposeResultCancelled:
                        resultMessage = @"Message canceled.";
                        break;
                case MFMailComposeResultSaved:
                        resultMessage = @"Message saved.";
                        break;
                case MFMailComposeResultSent:
                        resultMessage = @"Message sent.";
                        break;
                case MFMailComposeResultFailed:
                        resultMessage = @"Message failed.";
                        break;
                default:
                        resultMessage = @"Message not sent.";
                        break;
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];                //leg20140205 - 1.2.7
		
		[self alertEmailStatus:resultMessage];
}

#pragma mark UIAlertView

- (void)alertEmailStatus:(NSString*)alertMessage
{
	// open an alert with just an OK button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Status" message: alertMessage
							delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];	
}

- (void)alertOKCancelAction:(NSString*)alertMessage
{
	// open a alert with an OK and cancel button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message: alertMessage
							delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
	[alert show];
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{	
#pragma unused (actionSheet)

	// use "buttonIndex" to decide your action
	//
	// the user clicked one of the OK/Cancel buttons
	if (buttonIndex == 0)
	{
		//NSLog(@"Cancel");
	} else {
		//NSLog(@"OK");
		[self sendEmail];
	}
}

#pragma mark UIActionSheet

- (void)dialogOKCancelAction
{
	// open a dialog with an OK and cancel button
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"UIActionSheet <title>"
									delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Proceed" otherButtonTitles:nil];
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	[actionSheet showInView:self.view]; // show from our table view (pops up in the middle of the table)
}

//- (void)dismissWithClickedButtonIndex:(NSInteger)buttonIndex animated:(BOOL)animated


#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
#pragma unused (actionSheet)

	// the user clicked one of the OK/Cancel buttons
	if (buttonIndex == 0)
	{
		[self sendEmail];
	}
	else
	{
		;		// user tapped "Cancel" button
	}	
}

@end
