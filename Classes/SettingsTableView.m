//
//  SettingsTableView.m
//  TopXNotes
//
//  Created by Lewis Garrett on 3/4/10.
//  Copyright 2010 Iota. All rights reserved.
//

#import "SettingsTableView.h"
#import "BackupSettingsViewController.h"
#import "NoteSettingsViewController.h"
#import "AboutViewController.h"
#import "PurchaseUpgradeViewController.h"                                       //leg20140325 - Free 1.5.0
#import "NSMutableString+EmailEncodingExtensions.h"
#import "TopXNotesAppDelegate.h"                                                //leg20140325 - Free 1.5.0
#import "Constants.h"                                                           //leg20140325 - Free 1.5.0
#import "UIDeviceHardware.h"                                                    //leg20210716 - TopXNotesFree 1.6

@interface SettingsTableView () {
    NSArray *_products;                                                         //leg20140325 - Free 1.5.0
    NSNumberFormatter * _priceFormatter;                                        //leg20140325 - Free 1.5.0
}
@property (nonatomic, strong) IBOutlet UIView *contentView;                     //leg20140320 - Free 1.5.0

@end

@implementation SettingsTableView {
// Remove iAD support.                                                          //leg20210716 - TopXNotesFree 1.6
//    ADBannerView *_bannerView;                                                  //leg20140320 - Free 1.5.0
    BOOL    upgradedTopxFree;                                                   //leg20140325 - Free 1.5.0
}

// Note that it is necessary to change the cell connections in IB when cells    //leg20101219 - 1.0.3
//	are added, removed, or re-ordered.											//leg20101219 - 1.0.3
@synthesize model, cell0, cell0a, cell1, cell1a,cell2, cell3, cell4;            //leg20140325 - Free 1.5.0

- (void)viewDidLoad {
	
    [super viewDidLoad];

	// Add some wallpaper
	//self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"XPalm.png"]];
	//self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"XPalmWithBorder.png"]];
    //self.view.backgroundColor = [UIColor colorWithWhite:.5 alpha:1];		//gray close to steel

    // The above bg color setting methods no longer work as of Base SDK iOS     //leg20140205 - 1.2.7
    //  7.0.  I found that it was necessary to set the tableView's
    //  BackgroundView to nil in order for it to work in iOS 6.0.
    [self.view setBackgroundColor:[UIColor colorWithWhite:.5 alpha:1]];	//gray close to steel
    [self.tableView setBackgroundView:nil];

    // Navigation Bar needs to be NOT translucent on iOS 6 in order for the     //leg20140205 - 1.2.7
    //  picker views to be offset under the Navigation Bar, while on iOS 7 if
    //  the Navigation Bar is NOT translucent, it is solid black and different
    //  from all the other Navigation Bars.
//    if (IS_OS_7_OR_LATER)
//        self.navigationController.navigationBar.translucent = YES;
    
	modalAboutViewController = [[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
	modalAboutViewController.model = self.model;
	modalAboutViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;

// Remove iAD support.                                                          //leg20210716 - TopXNotesFree 1.6
//    // Suppress iAD in this view until a way to suppress just Landscape iAds    //leg20140417 - Free 1.5.0
//    //  because in Landscape orientation, the ad obscures the bottom
//    //  element of the Settings table view.
//    return;
//
//    // On iOS 6 ADBannerView introduces a new initializer,                      //leg20140320 - Free 1.5.0
//    //  use it when available.
//    if ([ADBannerView instancesRespondToSelector:@selector(initWithAdType:)]) {
//        _bannerView = [[ADBannerView alloc] initWithAdType:ADAdTypeBanner];
//    } else {
//        _bannerView = [[ADBannerView alloc] init];
//    }
//    _bannerView.delegate = self;
//
//    [self.view addSubview:_bannerView];                                         //leg20140320 - Free 1.5.0
}

- (void)viewWillAppear:(BOOL)animated                                           //leg20140325 - Free 1.5.0
{
#pragma unused (animated)
    
	// Get the upgrade status of TopXNotes Free.                                //leg20140325 - Free 1.5.0
	id appDelegate = [[UIApplication sharedApplication] delegate];
	upgradedTopxFree = [(TopXNotesAppDelegate*)appDelegate upgradeWasPurchased];
    
    [super viewWillAppear:animated];                                             //leg20140320 - Free 1.5.0
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];                                             //leg20140320 - Free 1.5.0
// Remove iAD support.                                                          //leg20210716 - TopXNotesFree 1.6
//    [self layoutAnimated:NO];                                                   //leg20140320 - Free 1.5.0
}

- (void)viewWillDisappear:(BOOL)animated {                                      //leg20140325 - Free 1.5.0
#pragma unused (animated)
    
    [super viewWillDisappear:animated];                                         //leg20150114 - Free 1.5.0

}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];                                          //leg20140320 - Free 1.5.0
}

#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_6_0
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation //leg20140320 - Free 1.5.0
{
#pragma unused (interfaceOrientation)

    return YES;
}
#endif

- (NSUInteger)supportedInterfaceOrientations                                    //leg20140320 - Free 1.5.0
{
    return UIInterfaceOrientationMaskAll;
}

- (void)viewDidUnload {
    
    [super viewDidUnload];                                                      //leg20150114 - Free 1.5.0
    
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

// Remove iAD support.                                                          //leg20210716 - TopXNotesFree 1.6
//#pragma mark - iAd
//
//- (void)viewDidLayoutSubviews                                                   //leg20140320 - Free 1.5.0
//{
//    [self layoutAnimated:[UIView areAnimationsEnabled]];
//}
//
//- (void)bannerViewDidLoadAd:(ADBannerView *)banner
//{
//#pragma unused (banner)
//
//    [self layoutAnimated:YES];
//}
//
//- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error  //leg20140320 - Free 1.5.0
//{
//#pragma unused (error, banner)
//
//    [self layoutAnimated:YES];
//}
//
//- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave //leg20140320 - Free 1.5.0
//{
//#pragma unused (banner, willLeave)
//
////    [self stopTimer];
//    return YES;
//}
//
//- (void)bannerViewActionDidFinish:(ADBannerView *)banner                        //leg20140320 - Free 1.5.0
//{
//#pragma unused (banner)
//
////    [self startTimer];
//}
//
//- (void)layoutAnimated:(BOOL)animated                                           //leg20140320 - Free 1.5.0
//{
//    // As of iOS 6.0, the banner will automatically resize itself based on its width.
//    // To support iOS 5.0 however, we continue to set the currentContentSizeIdentifier appropriately.
//    CGRect contentFrame = self.view.bounds;
//    if (contentFrame.size.width < contentFrame.size.height) {
//        _bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
//    } else {
//        _bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
//    }
//
//    CGRect bannerFrame = _bannerView.frame;
//    if (_bannerView.bannerLoaded) {
//        contentFrame.size.height -= _bannerView.frame.size.height;
//        bannerFrame.origin.y = contentFrame.size.height;
//    } else {
//        bannerFrame.origin.y = contentFrame.size.height;
//    }
//
//    [UIView animateWithDuration:animated ? 0.25 : 0.0 animations:^{
//        _contentView.frame = contentFrame;
//        [_contentView layoutIfNeeded];
//        _bannerView.frame = bannerFrame;
//    }];
//}

// user clicked the "i" button, present a modal UIViewController
- (IBAction)modalViewAction:(id)sender
{
#pragma unused (sender)

	// present as a modal child or overlay view
    [[self navigationController] presentViewController:modalAboutViewController animated:YES completion:nil];  //leg20140205 - 1.2.7
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#pragma unused (tableView)

    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#pragma unused (tableView, section)

    return 5;   // "Buy upgrade" is last cell.
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma unused (tableView)

switch (indexPath.row) {
		case 0:
			{
                if (upgradedTopxFree) {                                         //leg20140325 - Free 1.5.0
                    cell0.textLabel.text = @"Note Preferences";
                    cell0.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell0.textLabel.textAlignment = NSTextAlignmentCenter;      //leg20140205 - 1.2.7
                    cell0.textLabel.adjustsFontSizeToFitWidth = YES;            //leg20120405 - 1.2.0
                    return cell0;
               }else {
                   cell0a.textLabel.text = @"Note Preferences";
                   cell0a.detailTextLabel.text = @"Upgrade to unlock this feature";
                   cell0a.textLabel.textAlignment = NSTextAlignmentCenter;
                   cell0a.textLabel.adjustsFontSizeToFitWidth = YES;
                   return cell0a;
                }
			}
			break;
			
		case 1:
			{
                if (upgradedTopxFree) {                                        //leg20140325 - Free 1.5.0
                    cell1.textLabel.text = @"Back-up/Restore Notepad";
                    cell1.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell1.textLabel.textAlignment = NSTextAlignmentCenter;      //leg20140205 - 1.2.7
                    cell1.textLabel.adjustsFontSizeToFitWidth = YES;            //leg20120405 - 1.2.0
                    return cell1;
                }else {
                    cell1a.textLabel.text = @"Back-up/Restore Notepad";
                    cell1a.detailTextLabel.text = @"Upgrade to unlock this feature";
                    cell1a.textLabel.textAlignment = NSTextAlignmentCenter;
                    cell1a.textLabel.adjustsFontSizeToFitWidth = YES;
                    return cell1a;
                }
			}
			break;
			
		case 2:
			{
				cell2.textLabel.text = @"Contact Tropical Software";
				cell2.textLabel.textAlignment = NSTextAlignmentCenter;          //leg20140205 - 1.2.7
                cell2.textLabel.adjustsFontSizeToFitWidth = YES;                //leg20120405 - 1.2.0
				return cell2;
			}
			break;

        case 3:
            {
//                cell3.textLabel.text = @"About TopXNotes";
                cell3.textLabel.text = [NSString stringWithFormat:@"About %@",
                                        [self infoValueForKey:@"CFBundleDisplayName"]]; //leg20150318 - 1.5.0
                cell3.textLabel.textAlignment = NSTextAlignmentCenter;          //leg20140205 - 1.2.7
                cell3.textLabel.adjustsFontSizeToFitWidth = YES;                //leg20120405 - 1.2.0
                return cell3;
            }
        break;
        
		case 4:                                                                 //leg20140325 - Free 1.5.0
			{
//                // Buy upgrade cell, not present once bought.
//                cell4.textLabel.text = @"Upgrade not available!";
//                SKProduct * product;
//                if ([_products count] > 0) {
//                    product = (SKProduct *) _products[0];
//                
//                    if (product) {
//                        [_priceFormatter setLocale:product.priceLocale];
//                        cell4.textLabel.text = [NSString stringWithFormat:@"%@ for %@",
//                                                product.localizedTitle,
//                                                [_priceFormatter stringFromNumber:product.price]];
//                    }
//                } else {
//                    NSLog(@"Products count: %d", [_products count]);
//                }

                cell4.textLabel.text = @"Upgrades";
				cell4.textLabel.textAlignment = NSTextAlignmentCenter;
                cell4.textLabel.adjustsFontSizeToFitWidth = YES;
				return cell4;
			}
			break;
			
		default:
			return cell3;			// About box
			break;
	}
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma unused (tableView)

    // Navigation logic may go here. Create and push another view controller.
	switch (indexPath.row) {
		case 0:
			{
				// Note Preferences view
                if (upgradedTopxFree) {                                         //leg20140325 - Free 1.5.0
                    NoteSettingsViewController *noteSettingsViewController = [[NoteSettingsViewController alloc] initWithNibName:@"NoteSettingsViewController" bundle:nil];
                    noteSettingsViewController.model = self.model;
                    [self.navigationController pushViewController:noteSettingsViewController animated:YES];
                }
			}
			break;

		case 1:
			{
				// Backup/Restore settings view
                if (upgradedTopxFree) {                                         //leg20140325 - Free 1.5.0
                    BackupSettingsViewController *backupSettingsViewController = [[BackupSettingsViewController alloc] initWithNibName:@"BackupSettingsView" bundle:nil];
                    backupSettingsViewController.model = self.model;
                    [self.navigationController pushViewController:backupSettingsViewController animated:YES];
                }
			}
			break;

		case 2:
			{
				// Contact Tropical Software Support
				[self sendEmail];
			}
			break;

		case 3:                                                                 //leg20140325 - Free 1.5.0
            {
                // Present About box as a modal child or overlay view.
                modalAboutViewController = [[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
                modalAboutViewController.model = self.model;
                modalAboutViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                
                // On iOS 13 or greater must force full screen presentation if  //leg20210715 - TopXNotesFree 1.6
                //  views are in .xibs.
                modalAboutViewController.modalPresentationStyle = UIModalPresentationFullScreen;
                
                [[self navigationController] presentViewController:modalAboutViewController animated:YES completion:nil];  //leg20140205 - 1.2.7
            }
			break;
            
		case 4:                                                                 //leg20140325 - Free 1.5.0
			{
				// Present Purchase Upgrade as a modal child or overlay view.
//                modalPurchaseUpgradeViewController = [[PurchaseUpgradeViewController alloc] initWithNibName:@"PurchaseUpgradeViewController" bundle:nil];
//                modalPurchaseUpgradeViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
//                [[self navigationController] presentViewController:modalPurchaseUpgradeViewController animated:YES completion:nil];

                PurchaseUpgradeViewController *purchaseUpgradeVC = [[PurchaseUpgradeViewController alloc] initWithNibName:@"PurchaseUpgradeViewController" bundle:nil];
                [self.navigationController pushViewController:purchaseUpgradeVC animated:YES];
			}
			break;

		default:
			break;

	}
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



#pragma mark - Contact Tropical Support 

// fetch objects from our bundle based on keys in our Info.plist
- (id)infoValueForKey:(NSString*)key
{
	if ([[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key])
		return [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key];
	return [[[NSBundle mainBundle] infoDictionary] objectForKey:key];
}

// old way of sending Email before MFMailComposeViewController in iPhone OS 3.0
- (void)launchMailAppOnDevice
{
	//Note *note = [model getNoteForIndex:noteIndex];

	// Assemble the Email
	//NSString *subjectPrefix = @"Re: TopXNotes Note --> \"";
	//NSString *completeSubject = [subjectPrefix stringByAppendingString:note.title];
	//completeSubject = [completeSubject stringByAppendingString:@"\""];
	NSString *completeSubject = @"Re: Contact Tropical Software Support";
	
	NSMutableString *subject = [NSMutableString stringWithString:completeSubject];
	//NSMutableString *body = [NSMutableString stringWithString:note.noteText];
	//NSMutableString *body = @" ";
	NSString *versionInfo = [self infoValueForKey:@"CFBundleGetInfoString"];

	//NSMutableString *body = [NSMutableString stringWithString:@" "];
	NSMutableString *body = [NSMutableString stringWithFormat:@"TopXNotes touch %@", versionInfo];

	// encode the strings for email
	[subject encodeForEmail];
	[body encodeForEmail];

	NSString *emailMsg = [NSMutableString stringWithFormat:@"mailto:support@tropic4.com?subject=%@&body=%@", subject, body];
	NSURL *encodedURL = [NSURL URLWithString:emailMsg];
		 
	// Send the assembled message to Mail!
	[[UIApplication sharedApplication] openURL:encodedURL];
}

- (void)sendEmail
{

	// This can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
//	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
//	if (mailClass != nil)
    // Fix Issue #001 - Keyboard messed up on iPad Pro only                     //leg20210818 - TopXNotesFree 1.6
    //  For iPad Pro default to old Email method - launch Mail instead of using
    //  mail composer class.
    NSString *model = [[[UIDeviceHardware alloc] init] platformString];
    Boolean IsIPadPro = CFStringHasPrefix((CFStringRef)model, (CFStringRef)@"iPad Pro");
    NSLog(@"Current device is: %@", model);                                     //leg20210818 - TopXNotesFree 1.6

    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil && !IsIPadPro)                                         
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self displayComposerSheet];
		}
		else
		{
			[self launchMailAppOnDevice];
		}
	}
	else
	{
		[self launchMailAppOnDevice];
	}
}

-(void)displayComposerSheet 
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
        
	//Note *note = [model getNoteForIndex:noteIndex];

	// Set up recipients
	NSArray *toRecipients = [NSArray arrayWithObject:@"support@tropic4.com"]; 
	//NSArray *toRecipients = [NSArray arrayWithObject:@"mac@goofyfooter.com"]; 
	//NSArray *ccRecipients = [NSArray arrayWithObjects:@"second@example.com", @"third@example.com", nil]; 
	//NSArray *bccRecipients = [NSArray arrayWithObject:@"fourth@example.com"]; 
	
	[picker setToRecipients:toRecipients];
	//[picker setCcRecipients:ccRecipients];	
	//[picker setBccRecipients:bccRecipients];

	// Assemble the Email - Don't set any recipients, let user do during composition
	NSString *completeSubject = @"Re: Contact Tropical Software Support";
	//NSString *subjectPrefix = @"Re: Contact Tropical Software Support \"";
	//NSString *completeSubject = [subjectPrefix stringByAppendingString:note.title];
	//completeSubject = [completeSubject stringByAppendingString:@"\""];
	
	NSMutableString *subject = [NSMutableString stringWithString:completeSubject];
	//NSMutableString *body = [NSMutableString stringWithString:note.noteText];
	//NSMutableString *body = [NSMutableString stringWithString:@""];
	NSString *versionInfo = [self infoValueForKey:@"CFBundleGetInfoString"];
	//NSMutableString *body = [NSMutableString stringWithString:versionInfo];
	NSMutableString *body = [NSMutableString stringWithFormat:@"TopXNotes touch %@", versionInfo];

	[picker setSubject:subject];
	[picker setMessageBody:body isHTML:NO];
	
	// Present the composer
    [self presentViewController:picker animated:YES completion:nil];            //leg20140205 - 1.2.7
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{       
#pragma unused (controller, error)

	NSString *resultMessage;
        // Notifies users about errors associated with the interface
        switch (result)
        {
                case MFMailComposeResultCancelled:
                        resultMessage = @"Message canceled.";
                        break;
                case MFMailComposeResultSaved:
                        resultMessage = @"Message saved.";
                        break;
                case MFMailComposeResultSent:
                        resultMessage = @"Message sent.";
                        break;
                case MFMailComposeResultFailed:
                        resultMessage = @"Message failed.";
                        break;
                default:
                        resultMessage = @"Message not sent.";
                        break;
        }

        [self dismissViewControllerAnimated:YES completion:nil];                //leg20140205 - 1.2.7
		
		[self alertEmailStatus:resultMessage];
}

#pragma mark UIAlertView

- (void)alertEmailStatus:(NSString*)alertMessage
{
	// open an alert with just an OK button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Status" message: alertMessage
							delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];	
}

- (void)alertOKCancelAction:(NSString*)alertMessage
{
	// open a alert with an OK and cancel button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message: alertMessage
							delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
	[alert show];
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{	
#pragma unused (actionSheet)

	// use "buttonIndex" to decide your action
	//
	// the user clicked one of the OK/Cancel buttons
	if (buttonIndex == 0)
	{
		//NSLog(@"Cancel");
	} else {
		//NSLog(@"OK");
		[self sendEmail];
	}
}


#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
#pragma unused (actionSheet)

	// the user clicked one of the OK/Cancel buttons
	if (buttonIndex == 0)
	{
		[self sendEmail];
	}
	else
	{
		;		// user tapped "Cancel" button
	}	
}

@end

