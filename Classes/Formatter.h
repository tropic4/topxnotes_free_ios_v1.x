//
//  Formatter.h
//  GasTracker
//
//  Created by Rich Warren on 11/10/08.
//  Copyright 2008 Freelance Mad Science. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface NSString (Formatter) 

+ (NSString*)shortDate:(NSDate*)date;
+ (NSString*)longDate:(NSDate*)date;
+ (NSString*)decimal:(double)value;
+ (NSString*)currency:(double)value;
+ (double) parseDecimal:(NSString*)string;
+ (double) parseCurrency:(NSString*)string;
	
@end
