//
//  TopXNotesAppDelegate.h
//  NotesTopX
//
//  Created by Lewis Garrett on 4/5/09.
//  Copyright Tropical Software, Inc. 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Model;
@class SyncNotePad;

@interface TopXNotesAppDelegate : NSObject <UIApplicationDelegate,
											UITabBarControllerDelegate,
											UIActionSheetDelegate, NSNetServiceDelegate>	//leg20110523 - 1.0.4
{
    UIWindow *window;
    UITabBarController *tabBarController;
	
	IBOutlet Model *model;														//leg20110523 - 1.0.4
	BOOL syncEnabled;															//leg20110523 - 1.0.4
	BOOL firstBecameActive;														//leg20110523 - 1.0.4
	SyncNotePad *syncNotePad;
	
	NSMutableDictionary *savedSettingsDictionary;								//leg20110523 - 1.0.4
}

@property (nonatomic, strong) IBOutlet SyncNotePad *syncNotePad;				//leg20110523 - 1.0.4
@property (nonatomic, strong) IBOutlet Model *model;							//leg20110523 - 1.0.4

@property (nonatomic, strong) IBOutlet UIWindow *window;
@property (nonatomic, strong) IBOutlet UITabBarController *tabBarController;
@property BOOL upgradeWasPurchased;                                             //leg20140325 - Free 1.5.0

@end

