//
//  TopXNotesIAPHelper.h
//  TopXNotes Free
//
//  Created by Lewis Garrett on 3/25/14.
//  Copyright (c) 2014 Tropical Software, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IAPHelper.h"

@interface TopXNotesIAPHelper : IAPHelper

+ (TopXNotesIAPHelper *)sharedInstance;

@end
