//
//  TopXNotesIAPHelper.h
//  TopXNotes Free
//
//  Created by Lewis Garrett on 3/25/14.
//  Copyright (c) 2014 Tropical Software, Inc. All rights reserved.
//

#import "TopXNotesIAPHelper.h"

@implementation TopXNotesIAPHelper

+ (TopXNotesIAPHelper *)sharedInstance {
    static dispatch_once_t once;
    static TopXNotesIAPHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
//                                      @"com.tropic4.topxnoteslite.drummerrage",
//                                      @"com.tropic4.topxnoteslite.itunesconnectrage",
//                                      @"com.tropic4.topxnoteslite.nightlyrage",
//                                      @"com.tropic4.topxnoteslite.studylikeaboss",
//                                      @"com.tropic4.topxnoteslite.updogsadness",
                                      @"com.tropic4.topxnoteslite.upgradetopxfree",
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}

@end