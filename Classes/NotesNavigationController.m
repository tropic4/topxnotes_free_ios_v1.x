//
//  NotesNavigationController.m
//  NotesTopX
//
//  Created by Lewis Garrett on 4/7/09.
//  Copyright 2009 Iota. All rights reserved.
//

#import "NotesNavigationController.h"
//#import "AddNoteViewController.h"
#import "NoteViewController.h"
#import "Model.h"


@implementation NotesNavigationController
@synthesize model;
@synthesize notePadViewController;

-(IBAction)addNote {
    // Eliminate AddNoteViewController, make NoteViewController double duty.        //leg20130110 - 1.2.2
    //  noteIndex == -1 signals adding new note to NoteViewController.
	NoteViewController* noteViewController = [[NoteViewController alloc] initWithNibName:@"NoteView" noteIndex:-1];
	
    noteViewController.model = self.model;

    [self pushViewController:noteViewController animated: YES];
}

// On iOS 13 or greater must override so that appearance methods are called if  //leg20210715 - TopXNotesFree 1.6
//  views are in .xibs.
- (BOOL)shouldAutomaticallyForwardAppearanceMethods {
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
#pragma unused (interfaceOrientation)
    return YES;
}


@end
