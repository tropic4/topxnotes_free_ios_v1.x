//
//  SettingsTableView.h
//  TopXNotes
//
//  Created by Lewis Garrett on 3/4/10.
//  Copyright 2010 Iota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "PurchaseUpgradeViewController.h"                                       //leg20140325 - Free 1.5.0
// Remove iAD support.                                                          //leg20210716 - TopXNotesFree 1.6
//#import <iAd/iAd.h>                                                             //leg20140320 - Free 1.5.0

@class Model;
@class AboutViewController;

@interface SettingsTableView : UITableViewController <UIAlertViewDelegate,
// Remove iAD support.                                                          //leg20210716 - TopXNotesFree 1.6
//                                                        ADBannerViewDelegate,   //leg20140320 - Free 1.5.0
                                                        MFMailComposeViewControllerDelegate> {

	IBOutlet Model			*model;

    UITableViewCell *cell0;
    UITableViewCell *cell0a;    // Locked Note Preferences row.                 //leg20140325 - Free 1.5.0
    UITableViewCell *cell1;
    UITableViewCell *cell1a;	// Locked Backup/Restore row.                   //leg20140325 - Free 1.5.0
	UITableViewCell *cell2;
    UITableViewCell *cell3;                                                     //leg20110415 - 1.0.4
    UITableViewCell *cell4;		// Purchase Upgrade row.                        //leg20140325 - Free 1.5.0

	AboutViewController	*modalAboutViewController;
    PurchaseUpgradeViewController	*modalPurchaseUpgradeViewController;        //leg20140325 - Free 1.5.0
}

@property (nonatomic, strong) Model *model;
@property (nonatomic, strong) IBOutlet UITableViewCell *cell0;
@property (nonatomic, strong) IBOutlet UITableViewCell *cell0a;                 //leg20140325 - Free 1.5.0
@property (nonatomic, strong) IBOutlet UITableViewCell *cell1;
@property (nonatomic, strong) IBOutlet UITableViewCell *cell1a;                 //leg20140325 - Free 1.5.0
@property (nonatomic, strong) IBOutlet UITableViewCell *cell2;
@property (nonatomic, strong) IBOutlet UITableViewCell *cell3;                  //leg20110415 - 1.0.4
@property (nonatomic, strong) IBOutlet UITableViewCell *cell4;                  //leg20140325 - Free 1.5.0

- (void)sendEmail;

// Alerts
- (void)alertEmailStatus:(NSString*)alertMessage;

-(void)displayComposerSheet;
-(void)launchMailAppOnDevice;
@end
