//
//  IAPHelper.h
//  TopXNotes Free
//
//  Created by Lewis Garrett on 3/25/14.
//  Copyright (c) 2014 Tropical Software, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
// Add to the top of the file
#import <StoreKit/StoreKit.h>

UIKIT_EXTERN NSString *const IAPHelperProductPurchasedNotification;
UIKIT_EXTERN NSString *const IAPHelperFailedTransactionNotification;            //leg20140325 - Free 1.5.0

typedef void (^RequestProductsCompletionHandler)(BOOL success, NSArray * products);

@interface IAPHelper : NSObject

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers;
- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler;

// Add two new method declarations
- (void)buyProduct:(SKProduct *)product;
- (BOOL)productPurchased:(NSString *)productIdentifier;

- (void)restoreCompletedTransactions;

@end