//
//  PurchaseUpgradeViewController.m
//  TopXNotes
//
//  Created by Lewis Garrett on 3/26/14.
//  Copyright Tropical Software 2014. All rights reserved.
//
//leg20140325 - Free 1.5.0

#import <QuartzCore/QuartzCore.h>
#import "TopXNotesAppDelegate.h"
#import "PurchaseUpgradeViewController.h"
#import "UpgradeFeaturesViewController.h"                                       //leg20140417 - Free 1.5.0
#import "SVProgressHUD.h"
#import <StoreKit/StoreKit.h>
#import "TopXNotesIAPHelper.h"
#import "Constants.h"

@interface PurchaseUpgradeViewController () {
    NSArray *_products;
    NSNumberFormatter * _priceFormatter;
}
@end

@implementation PurchaseUpgradeViewController {
    BOOL    upgradedTopxFree;                                                   //leg20140417 - Free 1.5.0
}

@synthesize tableView = _tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		// this will appear as the title in the navigation bar
		self.title = NSLocalizedString(@"Purchase Upgrade", @"");
	}
	
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Configure table of possible purchases (only 1 for now.)
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.backgroundView = nil;
    [self.view setBackgroundColor:[Constants waterColor]];
    if (IS_OS_7_OR_LATER)                                                       //leg20150114 - Free 1.5.0
        self.view.tintColor = [Constants controlsColor];

	// Create a footer view for the Apple-required "Restore Purchases" button.  //leg20140417 - Free 1.5.0
    //  Wrap it in a container to allow us to position it better.
	UIView *containerView =
        [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 44)];

//    UIButton *restoreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *restoreButton = [UIButton buttonWithType:UIButtonTypeSystem];     //leg20150114 - Free 1.5.0
    if (IS_OS_7_OR_LATER)                                                       //leg20150114 - Free 1.5.0
        restoreButton.tintColor = [Constants controlsColor];
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED > __IPHONE_6_0

    restoreButton.frame = CGRectMake(-20, 0, 200, 44);
#else
    restoreButton.frame = CGRectMake(10, 0, 200, 44);
#endif

    [restoreButton addTarget:self action:@selector(restoreTapped:) forControlEvents:UIControlEventTouchUpInside];
    [restoreButton setTitle:NSLocalizedString(@"Restore Purchases", @"") forState:UIControlStateNormal];
    restoreButton.titleLabel.font = [UIFont systemFontOfSize: 18];
	[containerView addSubview:restoreButton];
	self.tableView.tableFooterView = containerView;

    // Prepare price formatter.
    _priceFormatter = [[NSNumberFormatter alloc] init];
    [_priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [_priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewWillAppear:(BOOL)animated {
#pragma unused (animated)

    [super viewWillAppear:animated];                                            //leg20150114 - Free 1.5.0

    [viewUpgradeFeaturesButton setTitleColor:[Constants controlsColor] forState:UIControlStateNormal];  //leg20150114 - Free 1.5.0

	// Get the upgrade status of TopXNotes Free.                                //leg20140417 - Free 1.5.0
	id appDelegate = [[UIApplication sharedApplication] delegate];
	upgradedTopxFree = [(TopXNotesAppDelegate*)appDelegate upgradeWasPurchased];
    
    if (upgradedTopxFree) {
        viewUpgradeFeaturesButton.hidden = YES;
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(failedTransaction:) name:IAPHelperFailedTransactionNotification object:nil];

    if ([Constants validNetworkConnection]) {
//        [SVProgressHUD showWithStatus:@"Loading Products..."];
//        [self reload];
        // Insure UI code runs on main thread. On iOS 13 and 14 IAP was         //leg20210721 - TopXNotesFree 1.6
        //  crashing because UI updating was occurring on non-main threads.
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showWithStatus:@"Loading Products..."];
            [self reload];
         });
    }
}

- (void)viewDidAppear:(BOOL)animated
{
#pragma unused (animated)
    
    [super viewDidAppear:animated];                                             //leg20150114 - Free 1.5.0
    
	// do something here as our view re-appears
}

- (void) viewWillDisappear:(BOOL)animated
{
#pragma unused (animated)
    
    [super viewWillDisappear:animated];                                         //leg20150114 - Free 1.5.0
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];

//    [SVProgressHUD dismiss];
    // Insure UI code runs on main thread. On iOS 13 and 14 IAP was         //leg20210721 - TopXNotesFree 1.6
    //  crashing because UI updating was occurring on non-main threads.
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
     });
}

// Fix for broken iOS 6.0 autorotation.
- (BOOL)shouldAutorotate
{
    return NO;
}

// Fix for broken iOS 6.0 autorotation.
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning
{
    NSLog(@"*********** %@ did receive MEMORY warning *******************", [self description]);
    
    [super didReceiveMemoryWarning];
}

- (void)productPurchased:(NSNotification *)notification {
    
    NSString * productIdentifier = notification.object;
    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
//        if ([product.productIdentifier isEqualToString:productIdentifier]) {
//            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
//            *stop = YES;
//
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUPGRADED_FREE_KEY];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//
//            // Show the upgrade as purchased.
//            TopXNotesAppDelegate *appDelegate = (TopXNotesAppDelegate*)[[UIApplication sharedApplication] delegate];
//            appDelegate.upgradeWasPurchased = YES;
//
//            [SVProgressHUD dismissWithSuccess:@"Upgrade Purchased!"];
//        }
        // Insure UI code runs on main thread. On iOS 13 and 14 IAP was         //leg20210721 - TopXNotesFree 1.6
        //  crashing because UI updating was occurring on non-main threads.
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([product.productIdentifier isEqualToString:productIdentifier]) {
                [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
                *stop = YES;
                
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUPGRADED_FREE_KEY];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                // Show the upgrade as purchased.
                TopXNotesAppDelegate *appDelegate = (TopXNotesAppDelegate*)[[UIApplication sharedApplication] delegate];
                appDelegate.upgradeWasPurchased = YES;

                [SVProgressHUD dismissWithSuccess:@"Upgrade Purchased!"];
            }
         });
    }];
    
}

- (void)failedTransaction:(NSNotification *)notification {
    
    NSString * transactionError = notification.object;
    
    NSLog(@"Transaction failed: %@", transactionError);

//    [SVProgressHUD dismiss];
    // Insure UI code runs on main thread. On iOS 13 and 14 IAP was         //leg20210721 - TopXNotesFree 1.6
    //  crashing because UI updating was occurring on non-main threads.
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}

- (void)reload {
    _products = nil;
    [self.tableView reloadData];
    [[TopXNotesIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
//        if (success) {
//            _products = products;
//            [self.tableView reloadData];
//            [SVProgressHUD dismissWithSuccess:@"Products Loaded!"];
//        } else {
//            [SVProgressHUD dismissWithError:@"Loading Products Failed!"];
//        }
////        [self.refreshControl endRefreshing];
        // Insure UI code runs on main thread. On iOS 13 and 14 IAP was         //leg20210721 - TopXNotesFree 1.6
        //  crashing because UI updating was occurring on non-main threads.
        dispatch_async(dispatch_get_main_queue(), ^{
            if (success) {
                self->_products = products;
                [self.tableView reloadData];
                [SVProgressHUD dismissWithSuccess:@"Products Loaded!"];
            } else {
                [SVProgressHUD dismissWithError:@"Loading Products Failed!"];
            }
//            [self.refreshControl endRefreshing];

        });
     }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#pragma unused (tableView)
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#pragma unused (tableView, section)

    // Return the number of rows in the section.
    return _products.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellStyleSubtitle;

    SKProduct * product = (SKProduct *) _products[indexPath.row];
    cell.textLabel.text = product.localizedTitle;
    
    [_priceFormatter setLocale:product.priceLocale];
    cell.detailTextLabel.text = [_priceFormatter stringFromNumber:product.price];
    
    if ([[TopXNotesIAPHelper sharedInstance] productPurchased:product.productIdentifier]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.accessoryView = nil;
        cell.tintColor = [UIColor blackColor];                                  //leg20150114 - Free 1.5.0
    } else {
//        UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeSystem];     //leg20150114 - Free 1.5.0
        buyButton.frame = CGRectMake(0, 0, 72, 37);
        [buyButton setTitle:@"Buy" forState:UIControlStateNormal];
        if (IS_OS_7_OR_LATER)                                                   //leg20150114 - Free 1.5.0
            buyButton.tintColor = [Constants controlsColor];                    
        buyButton.tag = indexPath.row;
        [buyButton addTarget:self action:@selector(buyButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.accessoryView = buyButton;
    }
    
    return cell;
}

#pragma mark - Interaction

- (void)buyButtonTapped:(id)sender {
    
    UIButton *buyButton = (UIButton *)sender;
    SKProduct *product = _products[buyButton.tag];
    
//    [SVProgressHUD showWithStatus:@"Buying Upgrade..."];
    // Insure UI code runs on main thread. On iOS 13 and 14 IAP was             //leg20210721 - TopXNotesFree 1.6
    //  crashing because UI updating was occurring on non-main threads.
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD showWithStatus:@"Buying Upgrade..."];
     });
    
    NSLog(@"Buying %@...", product.productIdentifier);
    [[TopXNotesIAPHelper sharedInstance] buyProduct:product];
    
}

// Restore In-App Purchases.
- (void)restoreTapped:(id)sender {
#pragma unused (sender)

    [[TopXNotesIAPHelper sharedInstance] restoreCompletedTransactions];

}

- (IBAction)dismissAction:(id)sender
{
#pragma unused (sender)
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)showUpgradeFeaturesAction:(id)sender                                //leg20140417 - Free 1.5.0
{
#pragma unused (sender)

    UpgradeFeaturesViewController *upgradeFeaturesVC = [[UpgradeFeaturesViewController alloc] initWithNibName:@"UpgradeFeaturesViewController" bundle:nil];
    [self.navigationController pushViewController:upgradeFeaturesVC animated:YES];
}

@end
