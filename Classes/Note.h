//
//  Note.h
//  TopXNotes
//
//  Created by Lewis Garrett on 4/8/09.
//  Copyright 2009 Tropical Software. All rights reserved.
//
// Changes:
//
//leg20120911 - 1.7.5 - Added createDate to Note.
//leg20121120 - 1.2.2 - Added encryption/decryption fields.
//

@interface Note : NSObject <NSCoding> {
	NSString*	title;
	NSDate*		createDate;                                                     //leg20120911 - 1.7.5
	NSDate*		date;
	NSString*	noteText;
	NSData*     encryptedNoteText;                                              //leg20121120 - 1.2.2
	NSData*     encryptedPassword;                                              //leg20121120 - 1.2.2
	NSString*   passwordHint;                                                   //leg20121204 - 1.3.0
	NSString*   decryptedPassword;                                              //leg20121204 - 1.3.0
	NSNumber*	noteID;
	NSNumber*	groupID;
	NSNumber*	syncFlag;
	NSNumber*	needsSyncFlag;
	NSNumber*	protectedNoteFlag;                                              //leg20121120 - 1.2.2
	NSNumber*	decryptedNoteFlag;                                              //leg20121204 - 1.3.0
}

@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSDate* createDate;                               //leg20120911 - 1.7.5
@property (nonatomic, strong) NSDate* date;
@property (nonatomic, strong) NSString* noteText;
@property (nonatomic, strong) NSData* encryptedNoteText;                        //leg20121120 - 1.2.2
@property (nonatomic, strong) NSData* encryptedPassword;                        //leg20121120 - 1.2.2
@property (nonatomic, strong) NSString* passwordHint;                           //leg20121204 - 1.3.0
@property (nonatomic, strong) NSString* decryptedPassword;                      //leg20121204 - 1.3.0
@property (nonatomic, strong) NSNumber* noteID;
@property (nonatomic, strong) NSNumber* groupID;
@property (nonatomic, strong) NSNumber* syncFlag;
@property (nonatomic, strong) NSNumber* needsSyncFlag;
@property (nonatomic, strong) NSNumber* protectedNoteFlag;                      //leg20121120 - 1.2.2
@property (nonatomic, strong) NSNumber* decryptedNoteFlag;                      //leg20121204 - 1.3.0

//// Experimental class function -- research how to properly write this.          //leg20121210
//+ (Note*) noteWithNote:(Note*)note;

- (id) initWithTitle:(NSString*) noteTitle
				text:(NSString*) textField 
				  withID:(NSNumber*) noteID
				  onDate:(NSDate*) entryDate;


- (id) initWithTitle:(NSString*) noteTitle          //leg20120911 - 1.7.5
                text:(NSString*) textField
              withID:(NSNumber*)noteIDField
              onDate:(NSDate*) entryDate
          createDate:(NSDate*) creationDate;

@end
