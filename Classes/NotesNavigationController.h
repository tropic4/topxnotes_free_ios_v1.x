//
//  NotesNavigationController.h
//  NotesTopX
//
//  Created by Lewis Garrett on 4/7/09.
//  Copyright 2009 Iota. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NotePadViewController.h"

@class Model;


@interface NotesNavigationController : UINavigationController {

	IBOutlet Model* model;
	IBOutlet NotePadViewController* notePadViewController;
}

@property (nonatomic, strong) Model *model;
@property (nonatomic, strong) NotePadViewController *notePadViewController;

-(IBAction)addNote;

@end
