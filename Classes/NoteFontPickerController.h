//
//  NoteFontPickerController.h
//  TopXNotes
//
// Abstract: Controller to managed a picker view displaying Fonts and Font Sizes.
//
//  Created by Lewis Garrett on 4/15/11.
//  Copyright 2011 Tropical Software. All rights reserved.
//

@class Model;
@interface NoteFontPickerController : NSObject <UIPickerViewDataSource, UIPickerViewDelegate,
												UIAlertViewDelegate, UIActionSheetDelegate> {

    IBOutlet UIPickerView	*pickerView;
    IBOutlet UILabel		*label;
    IBOutlet UIButton		*saveFontButton;
	IBOutlet Model			*model;

	NSMutableArray	*fontNamesArray;
	NSArray			*fontSizesArray;
}

@property (nonatomic, strong) NSMutableArray *fontNamesArray;
@property (nonatomic, strong) NSArray *fontSizesArray;
@property (nonatomic, strong) IBOutlet UIPickerView *pickerView;
@property (nonatomic, strong) IBOutlet UILabel *label;
@property (nonatomic, strong) IBOutlet UIButton	*saveFontButton;
@property (nonatomic, strong) Model *model;

- (IBAction)saveFontSelection;
- (void)saveFontSelection;
- (void)alertSimpleAction:(NSString*)alertMessage;
- (void)alertOKCancelAction:(NSString*)alertMessage;
- (NSString*)pathToAutoBackup:(NSString*)withFileName;
- (NSString*)pathToBackupData;

@end
